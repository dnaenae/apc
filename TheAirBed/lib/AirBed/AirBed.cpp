#include "Arduino.h"
#include "AirBed.h"

Valve::Valve(int pin){
  _pin=pin;
  pinMode(_pin, OUTPUT);
  digitalWrite(_pin, LOW);
}

bool Valve::openValve(){
  if(getValveStatus() == LOW){
    digitalWrite(_pin, HIGH);
    return true;
  }
  return false;
}

bool Valve::closeValve(){
  if(getValveStatus() == HIGH){
    digitalWrite(_pin, LOW);
    return true;
  }
  return false;
}

bool Valve::getValveStatus(){
  return digitalRead(_pin);
}

PressureSensor::PressureSensor(int pin){
  _pin=pin;
}

void PressureSensor::setBaseline(){
  _baseline = getPressure();
}

float PressureSensor::getBaseline(){
  return _baseline;
}

float PressureSensor::getPressure(){
  float pressure = 0;
  for(int i = 0; i < 10; i++){
    int pressureValue = analogRead(_pin);
    delay(5);
    pressure =+ (((pressureValue/1024.0)+0.095)/0.000009)/100;
  }
  pressure = pressure/10;
  return pressure - _baseline;
}

SFR::SFR(int pin, int diff){
  _pin = pin;
  _diff = diff;
  pressed = false;
}

bool SFR::isPressed(){
  int pressCount = 0;
  for(int i = 0; i < 10; i++){
    if(analogRead(_pin) > _diff){
      pressCount++;
    }
    delay(5);
  }
  if (pressCount > 4){
    return true;
  } else {
      return false;
  }
}

int SFR::getPressure(){
  return analogRead(_pin);
}
