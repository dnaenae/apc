#ifndef Airbed_h
#define AirBed_h

#include  "Arduino.h"

class Valve
{
public:
  Valve();
  Valve(int pin);
  bool openValve();
  bool closeValve();
  bool getValveStatus();
private:
  int _pin;
};

class PressureSensor
{
public:
  PressureSensor();
  PressureSensor(int pin);
  float getPressure();
  float getBaseline();
  void setBaseline();
private:
  int _pin;
  float _baseline;
};

class SFR
{
public:
  SFR(int pin, int diff);
  bool isPressed();
  int getPressure();
private:
  void calibrate();
  int _pin;
  int _diff;
  bool pressed;
};

#endif
