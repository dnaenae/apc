#include "Arduino.h"
#include "AirBed.h"
#include "FlexiTimer2.h"

enum Position {right, left, back, falser};
Position position;
int positionCounter = 0;
bool lOrR = false;

SFR sfrRight(A4,300);
SFR sfrLeft(A5,300);


Valve maValve(9);
Valve blokValve(8);
int maCounter = 0;

Valve valve1(10);
Valve valve2(11);
Valve valve3(12);
Valve valve4(13);

PressureSensor pressureSensor1(A3);
PressureSensor pressureSensor2(A2);
PressureSensor pressureSensor3(A1);
PressureSensor pressureSensor4(A0);

bool airIn;
bool airOut;

float pressure1;
float pressure2;
float pressure3;
float pressure4;

void calibrate(Valve valve, PressureSensor pressureSensor, Valve maValve, float pressure, String name){
  Serial.print(name);
  Serial.print(": ");
  if (pressureSensor.getPressure() > pressure && maValve.getValveStatus() == 1){
     valve.openValve();
     //Serial.print(name);
     Serial.println(" out");
     airOut = true;
   } else if (pressureSensor.getPressure() < pressure && maValve.getValveStatus() == 0){
     valve.openValve();
     //Serial.print(name);
     Serial.println(" in");
     airIn = true;
   } else {
     valve.closeValve();
     Serial.println(" Close");
   }
}

Position definePosition(){
  bool leftPressed = sfrLeft.isPressed();
  bool rightPressed = sfrRight.isPressed();

  if(leftPressed && rightPressed) {
    position = back;
    Serial.println("Position set to BACK");
    return back;
  } else if(leftPressed) {
    position = left;
    Serial.println("Position is LEFT");
    return left;
  } else if(rightPressed) {
    position = right;
    Serial.println("Position is RIGHT");
    return right;
  } else {
    position = falser;
    Serial.println("definePosition - ERROR: No position defined");
    return falser;
  }
}

void masterValveCyclus(){
  (maValve.getValveStatus()) ? maValve.closeValve() : maValve.openValve();
  (blokValve.getValveStatus()) ? blokValve.closeValve() : blokValve.openValve();

  if (maValve.getValveStatus()){
    Serial.println("Master valve open - letting air out");
  } else {
    Serial.println("Master valve closed - Letting air in");
  }

  //Serial.print("Master valve status changes to: ");
  //Serial.println(maValve.getValveStatus());
}

void setPressure(float v1, float v2, float v3, float v4){
  pressure1 = v1;
  pressure2 = v2;
  pressure3 = v3;
  pressure4 = v4;
}

void setPosition(){
  Serial.print("SETTING POSITION: ");
  definePosition();
  switch (position) {
    case right :
    //push to back
    Serial.println("Moving to back, (3, -1, -1, -1) ");
    setPressure(3, -1, -1, -1);
    break;
    case left :
    //push to back
    Serial.println("Moving to back, (-1, -1, -1, 3) ");
    setPressure(-1, -1, -1, 3);
    break;
    case back :
    //push to left or right
    Serial.println("Moving to left or right");
    lOrR ? setPressure(3, 3, -1, -1) : setPressure(-1, -1, 3, 3);
    break;
    case falser :
    Serial.println("setPosition - ERROR: No position defined");
    default :
    Serial.println("setPosition - ERROR: Something is wrong, default called");
    break;
  }
}

void setup() {
  Serial.begin(9600);
  Serial.println("Start");
  maValve.closeValve();
  blokValve.openValve();


  pressureSensor1.setBaseline();
  pressureSensor2.setBaseline();
  pressureSensor3.setBaseline();
  pressureSensor4.setBaseline();


  Serial.print("baseline 1: ");
  Serial.println(pressureSensor1.getBaseline());
  Serial.print("baseline 2: ");
  Serial.println(pressureSensor2.getBaseline());
  Serial.print("baseline 3: ");
  Serial.println(pressureSensor3.getBaseline());
  Serial.print("baseline 4: ");
  Serial.println(pressureSensor4.getBaseline());

  setPosition();
}

void loop() {

  maCounter++;
  if (maCounter == 3) {
    masterValveCyclus();
    maCounter = 0;
  }

  airIn = false;
  airOut = false;

  calibrate(valve1, pressureSensor1, maValve, pressure1, "1");
  calibrate(valve2, pressureSensor2, maValve, pressure2, "2");
  calibrate(valve3, pressureSensor3, maValve, pressure3, "3");
  calibrate(valve4, pressureSensor4, maValve, pressure4, "4");

  Serial.print("Left FSR: ");
  Serial.println(sfrLeft.getPressure());
  Serial.print("Right FSR: ");
  Serial.println(sfrRight.getPressure());

  positionCounter++;
  if (positionCounter == 20) {
    setPosition();
    positionCounter = 0;
  }

  delay(1000);
}
